#include <libgweather/gweather.h>

static void update_finish(GWeatherInfo *info, gpointer data)
{
	printf("update finished\n");
}

int main (int argc, char *argv[])
{
	GWeatherInfo *info;

	gtk_init (&argc, &argv);
	info = gweather_info_new(NULL,GWEATHER_FORECAST_ZONE);
	gweather_info_update(info);
	g_signal_connect(info, "updated", G_CALLBACK(update_finish), NULL);
	
	gtk_main ();
	return 0;
}
