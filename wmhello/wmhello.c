#include <WINGs/WINGs.h>

Display *display;
WMScreen *screen;
WMWindow *dockapp;
WMLabel *hello;

WMWindow *WMCreateDockapp(WMScreen *screen, const char *name, int argc, char **argv);

int main(int argc, char **argv)
{


	WMInitializeApplication("hello", &argc, argv);

	display = XOpenDisplay("");
	screen = WMCreateScreen(display, DefaultScreen(display));
	dockapp = WMCreateDockapp(screen, "", argc, argv);
	WMSetWidgetBackgroundColor(dockapp,WMCreateNamedColor(screen,"Black",True));

	hello = WMCreateLabel(dockapp);
	WMSetLabelText(hello,"Hello\nWorld!");
	WMSetLabelTextColor(hello,WMCreateNamedColor(screen,"Light sea green",True));
	WMSetWidgetBackgroundColor(hello,WMCreateNamedColor(screen,"Black",True));
	WMSetLabelTextAlignment (hello, WACenter);
	WMResizeWidget(hello,48,48);
	WMRealizeWidget(hello);

	WMMapSubwidgets(dockapp);
	WMMapWidget(dockapp);

	WMScreenMainLoop(screen);


}

WMWindow *WMCreateDockapp(WMScreen *screen, const char *name, int argc, char **argv)
{
	WMWindow *dockapp;
	XWMHints *hints;
	Display *display;
	Window window;
	
	display = WMScreenDisplay(screen);
	dockapp = WMCreateWindow(screen,name);
	WMRealizeWidget(dockapp);
	
	window = WMWidgetXID(dockapp);

	hints = XGetWMHints(display, window);
	hints->flags |= WindowGroupHint|IconWindowHint|StateHint;
	hints->window_group = window;
    	hints->icon_window = window;
    	hints->initial_state = WithdrawnState;

	XSetWMHints(display, window, hints);
	XFree(hints);

	XSetCommand(display, window, argv, argc);

	WMResizeWidget(dockapp,48,48);
	return dockapp;
}
