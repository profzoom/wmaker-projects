// gcc `pkg-config --cflags gtk+-3.0 --cflags gdk-x11-3.0 ` -o wmappindicator wmappindicator.c `pkg-config --libs gtk+-3.0 --libs gdk-x11-3.0`

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <X11/Xlib.h>
#include <gdk/gdkx.h> 

int
main (int   argc,
char *argv[])
{
  GtkBuilder *builder;
   
  GtkWidget *dockapp;
  GdkWindow *gdkDockapp;
  Window xDockapp;

  GdkDisplay *gdkDisplay;
  Display *xDisplay;

  XWMHints wmhints;

  gtk_init (&argc, &argv);

  builder = gtk_builder_new();
  gtk_builder_add_from_file(builder,"wmappindicator.glade",NULL);
  dockapp = GTK_WIDGET(gtk_builder_get_object(builder,"dockapp"));

  gtk_window_set_wmclass(GTK_WINDOW(dockapp),"foo","DockApp");

  gtk_widget_show(dockapp);

  gdkDockapp = gtk_widget_get_window(dockapp);
  xDockapp = GDK_WINDOW_XID(gdkDockapp);

  gdkDisplay = gdk_window_get_display(gdkDockapp);
  xDisplay = GDK_DISPLAY_XDISPLAY(gdkDisplay);

  wmhints.icon_window = xDockapp;
  wmhints.flags = IconWindowHint;
  XSetWMHints(xDisplay, xDockapp, &wmhints);  

  gtk_main ();

  return 0;
}
