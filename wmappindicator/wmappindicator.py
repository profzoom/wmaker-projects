#! /usr/bin/python

from gi.repository import Gtk,Gdk,GdkX11
from Xlib.display import Display
from Xlib import Xutil

builder = Gtk.Builder()
builder.add_from_file("wmappindicator.glade")
dockapp = builder.get_object("dockapp")
dockapp.connect("delete-event", Gtk.main_quit)

dockapp.set_wmclass("foo","Dockapp")

dockapp.show()

gdkDockapp = dockapp.get_window()
xDockapp = gdkDockapp.get_xid()

gdkDisplay = gdkDockapp.get_display()
xDisplay = gdkDisplay.get_xdisplay()

xDockapp.set_wm_hints(
    icon_window=xDockapp,
    flags=Xutil.IconWindowHint
)

Gtk.main()

#d.create_resource_object("window", xid), where d is Xlib.display.Display
