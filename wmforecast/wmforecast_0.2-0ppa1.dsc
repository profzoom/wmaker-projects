-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: wmforecast
Binary: wmforecast
Architecture: any
Version: 0.2-0ppa1
Maintainer: Doug Torrance <dtorrance@monmouthcollege.edu>
Homepage: http://wmforecast.sourceforge.net
Standards-Version: 3.9.5
Build-Depends: debhelper (>= 9), libxml2-dev, libcurl4-gnutls-dev, libwings-dev
Package-List: 
 wmforecast deb utils extra
Checksums-Sha1: 
 14e75215cc23e52cd0eafd78521bd7e0ac711711 188170 wmforecast_0.2.orig.tar.gz
 14d0368bb3f3dd720235433fbbfade8873574bd6 8741 wmforecast_0.2-0ppa1.debian.tar.gz
Checksums-Sha256: 
 da81c285bf211af002f8feaa3a6ccaae831eb54958e8c823cc36a6b7fc90d8df 188170 wmforecast_0.2.orig.tar.gz
 4d84cd536877241bcc7de56b5c13a0075a41b89b34c08995a6e13f2a9737077f 8741 wmforecast_0.2-0ppa1.debian.tar.gz
Files: 
 c0b214927fd673b241c7820f18648163 188170 wmforecast_0.2.orig.tar.gz
 a27d182351819217ddfa9c3881d3bf2b 8741 wmforecast_0.2-0ppa1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.14 (GNU/Linux)

iQEcBAEBAgAGBQJTTz74AAoJEEr/zw37nyKKQ+QIAK6aRiG8zTKZzYn1kpuOdaVL
uONnXajqSwrszigdfUnGe5x/hOTKKGxZUfSj5yfkZosTyBJbfHebeponcMPif030
UoFhhqpmqMZNXPZdkLKuqJzbyFAYlzvLtAnzD2PkRK3T52+U3MMDTVweKn2+MYov
F2XnhJmnOAmRUw3V7eu4CtmACIlr+X3LcntJcQ5zgVHgJB1uOWXLO8a2050zN6i7
ymnVIllSWgkzXEyGmpNOBeS/Y2QuvhKFPM/OLB87hOeSuPzLWkgt6ckrUhL/beVd
XCrb2DKooGb1ImTtcJkyJbGi/wFt3FMt0QsP7Jb7zBG0dtmfryOtGZ5s1EK87R8=
=tu9P
-----END PGP SIGNATURE-----
