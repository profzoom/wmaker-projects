/* $Id: basic.c,v 1.10 2003/02/10 12:35:03 dalroi Exp $
 *
 * Copyright (c) 2002 Alban G. Hertroys
 * 
 * Basic example of libDockapp usage
 *
 * This dockapp will draw a rectangle with a
 * bouncing ball in it.
 */

/* also includes Xlib, Xresources, XPM, stdlib and stdio */
#include <dockapp.h>

/*
 * Prototypes for local functions
 */
void drawRelief();
void destroy();

/*
 * M A I N
 */

int
main(int argc, char **argv)
{
    unsigned int x=1, y=1;
    Pixmap back;
    DACallbacks eventCallbacks = {
	destroy, /* destroy */
	NULL, /* buttonPress */
	NULL, /* buttonRelease */
	NULL, /* motion (mouse) */
	NULL, /* mouse enters window */
	NULL, /* mouse leaves window */
	NULL /* timeout */
    };

    /* provide standard command-line options */
    DAParseArguments(
	    argc, argv,	/* Where the options come from */
	    NULL, 0,	/* Our list with options - none as you can see */
	    "This is the help text for the basic example of how to "
	    "use libDockapp.\n",
	    "Basic example version 1.1");

    /* Tell libdockapp what version we expect it to be (a date from the
     * ChangeLog should do).
     */
    DASetExpectedVersion(20020126);

    DAOpenDisplay(
	    NULL	/* default display */,
	    argc, argv	/* needed by libdockapp */
    );
    DACreateIcon(
	    "daBasicExample"	/* WM_CLASS hint; don't use chars in [.?*: ] */,
	    48, 48		/* geometry of dockapp internals */,
	    argc, argv		/* needed by libdockapp */
    );

    /* The pixmap that makes up the background of the dockapp */
    back = DAMakePixmap();
    drawRelief(back);
    DASetPixmap(back);
    XFreePixmap(DADisplay, back);

    /* Respond to destroy and timeout events (the ones not NULL in the
     * eventCallbacks variable.
     */
    DASetCallbacks(&eventCallbacks);


    DAShow();    /* Show the dockapp window. */

    /* Process events and keep the dockapp running */
    DAEventLoop();

    /* not reached */
    exit(EXIT_SUCCESS);
}


void
drawRelief(Pixmap pixmap)
{
    XGCValues gcv;
    GC lightGrayGC, darkGrayGC;

    /* GC's */
    gcv.foreground = DAGetColor("Navy");
    XChangeGC(DADisplay, DAClearGC, GCForeground, &gcv);

    gcv.foreground = DAGetColor("lightGray");
    gcv.graphics_exposures = False;

    lightGrayGC	= XCreateGC(DADisplay, DAWindow,
	    GCForeground|GCGraphicsExposures, &gcv);

    gcv.foreground = DAGetColor("#222222");
    darkGrayGC	=  XCreateGC(DADisplay, DAWindow,
	    GCForeground|GCGraphicsExposures, &gcv);

    /* Drawing */
    XFillRectangle(DADisplay, pixmap, DAClearGC, 1, 1, 46, 46);

    XDrawLine(DADisplay, pixmap, darkGrayGC, 0, 0, 0, 46);
    XDrawLine(DADisplay, pixmap, darkGrayGC, 1, 0, 47, 0);

    XDrawLine(DADisplay, pixmap, lightGrayGC, 0, 47, 47, 47);
    XDrawLine(DADisplay, pixmap, lightGrayGC, 47, 1, 47, 46);

    /* Free the GC's, we don't use them anymore */
    XFreeGC(DADisplay, lightGrayGC);
    XFreeGC(DADisplay, darkGrayGC);
}



void
destroy(void)
{

fprintf(stderr, "Destroyed!\n");
    /* exit is done by libdockapp */
}
