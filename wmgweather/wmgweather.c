#include <WINGs/WINGs.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <libgweather/gweather.h>
#include <gtk/gtk.h>


#define color(c) WMCreateNamedColor(screen,c,True)

typedef struct {
	char *temp;
} Weather;

Weather *newWeather()
{
	Weather *w = malloc(sizeof(Weather));
	w->temp = NULL;
	return w;
}

void freeWeather(Weather *w)
{
	if (w->temp) free(w->temp);
	free(w);
}

void setTemp(Weather *w, const char *temp)
{
	w->temp = realloc(w->temp, strlen(temp) + 1);
	strcpy(w->temp, temp);
}

/* public domain string concatenation function by Solar Designer from popa3d */
char *concat(const char *s1, ...)
{
	va_list args;
	const char *s;
	char *p, *result;
	unsigned long l, m, n;
 
	m = n = strlen(s1);
	va_start(args, s1);
	while ((s = va_arg(args, char *))) {
		l = strlen(s);
		if ((m += l) < l) break;
	}
	va_end(args);
	if (s || m >= INT_MAX) return NULL;
 
	result = (char *)malloc(m + 1);
	if (!result) return NULL;
 
	memcpy(p = result, s1, n);
	p += n;
	va_start(args, s1);
	while ((s = va_arg(args, char *))) {
		l = strlen(s);
		if ((n += l) < l || n > m) break;
		memcpy(p, s, l);
		p += l;
	}
	va_end(args);
	if (s || m != n || p != result + n) {
		free(result);
		return NULL;
	}
 
	*p = 0;
	return result;
}

WMWindow *WMCreateDockapp(WMScreen *screen, const char *name, int argc, char **argv)
{
	Display *display;
	Window window;
	WMWindow *dockapp;
	XWMHints *hints;
	
	display = WMScreenDisplay(screen);
	dockapp = WMCreateWindow(screen,name);
	WMRealizeWidget(dockapp);
	
	window = WMWidgetXID(dockapp);

	hints = XGetWMHints(display, window);
	hints->flags |= WindowGroupHint|IconWindowHint|StateHint;
	hints->window_group = window;
    	hints->icon_window = window;
    	hints->initial_state = WithdrawnState;

	XSetWMHints(display, window, hints);
	XFree(hints);

	XSetCommand(display, window, argv, argc);

	WMResizeWidget(dockapp,56,56);
	return dockapp;
}

Weather *getWeather(void)
{
	Weather *weather;

	weather = newWeather();
	return weather;
}

static void update_finish(GWeatherInfo *w)
{
	printf("ding ding ding!\n");
//	printf("%s\n",gweather_info_get_temp(w));
}

const gchar *getIconFilename(char *iconName)
{
	GtkIconTheme *iconTheme;
	GtkIconInfo *icon;

	iconTheme = gtk_icon_theme_get_default();
	icon = gtk_icon_theme_lookup_icon (iconTheme,iconName,32,0);
	if (!icon) {
		iconTheme = gtk_icon_theme_new();
		gtk_icon_theme_set_custom_theme(iconTheme,"Tango");
		icon = gtk_icon_theme_lookup_icon (iconTheme,iconName,32,0);
	}
	return gtk_icon_info_get_filename(icon);
}

int main(int argc, char **argv)
{

	Display *display;
	const gchar *filename;
	GWeatherInfo *info;
	GWeatherLocation *location;
	RContext *context;
	RImage *image;
	Weather *weather;
	WMFrame *frame;
	WMLabel *icon;
	WMLabel *temp;
	WMPixmap *pixmap;	
	WMScreen *screen;
	WMWindow *dockapp;

	weather = getWeather();

	WMInitializeApplication("wmgweather", &argc, argv);

	display = XOpenDisplay("");
	screen = WMCreateScreen(display, DefaultScreen(display));
	dockapp = WMCreateDockapp(screen, "", argc, argv);

	frame = WMCreateFrame(dockapp);
	WMSetFrameRelief(frame,WRSunken);
	WMResizeWidget(frame,56,56);
	WMSetWidgetBackgroundColor(frame,color("black"));
	WMRealizeWidget(frame);

	temp = WMCreateLabel(frame);
	WMSetLabelText(temp,concat("50","°",NULL));
	WMSetWidgetBackgroundColor(temp,color("black"));
	WMSetLabelTextColor(temp,color("Light sea green"));
	WMSetLabelTextAlignment (temp, WACenter);
	WMResizeWidget(temp,52,14);
	WMMoveWidget(temp,2,40);
	WMRealizeWidget(temp);


	gtk_init(&argc, &argv);
	location = gweather_location_new_world(True);
	location = gweather_location_find_by_station_code(location,"KGBG");

	info = gweather_info_new(location,GWEATHER_FORECAST_STATE);
	gweather_info_update(info);

	g_signal_connect(info, "updated", G_CALLBACK(update_finish), NULL);


	context = WMScreenRContext(screen);
	image = RLoadImage(context,"weather-few-clouds.png",0);
//	image = RSmoothScaleImage(image,32,32);

	filename = getIconFilename("weather-few-clouds");
	printf("%s\n",filename);


	pixmap = WMCreatePixmapFromRImage(screen,image,0);

	icon = WMCreateLabel(frame);
	WMSetWidgetBackgroundColor(icon,color("black"));
	WMRealizeWidget(icon);
	
	WMSetLabelImage(icon,pixmap);
	WMSetLabelImagePosition(icon,WIPImageOnly);
	WMResizeWidget(icon,32,32);
	WMMoveWidget(icon,12,5);

	WMSetBalloonTextForView("foo",WMWidgetView(icon)); 

	WMMapWidget(dockapp);
	WMMapWidget(frame);
	WMMapSubwidgets(frame);


	WMScreenMainLoop(screen);
}
