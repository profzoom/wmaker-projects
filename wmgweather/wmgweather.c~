#include <WINGs/WINGs.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#define color(c) WMCreateNamedColor(screen,c,True)

typedef struct {
	char *temp;
} Weather;

Display *display;
WMScreen *screen;
WMWindow *dockapp;
WMFrame *frame;
WMLabel *temp;
WMLabel *icon;
WMPixmap *iconPixmap;

Weather *newWeather()
{
	Weather *w = malloc(sizeof(Weather));
	w->temp = NULL;
	return w;
}

void freeWeather(Weather *w)
{
	if (w->temp) free(w->temp);
	free(w);
}

void setTemp(Weather *w, const char *temp)
{
	w->temp = realloc(w->temp, strlen(temp) + 1);
	strcpy(w->temp, temp);
}

/* public domain string concatenation function by Solar Designer from popa3d */
char *concat(const char *s1, ...)
{
	va_list args;
	const char *s;
	char *p, *result;
	unsigned long l, m, n;
 
	m = n = strlen(s1);
	va_start(args, s1);
	while ((s = va_arg(args, char *))) {
		l = strlen(s);
		if ((m += l) < l) break;
	}
	va_end(args);
	if (s || m >= INT_MAX) return NULL;
 
	result = (char *)malloc(m + 1);
	if (!result) return NULL;
 
	memcpy(p = result, s1, n);
	p += n;
	va_start(args, s1);
	while ((s = va_arg(args, char *))) {
		l = strlen(s);
		if ((n += l) < l || n > m) break;
		memcpy(p, s, l);
		p += l;
	}
	va_end(args);
	if (s || m != n || p != result + n) {
		free(result);
		return NULL;
	}
 
	*p = 0;
	return result;
}


WMWindow *WMCreateDockapp(WMScreen *screen, const char *name, int argc, char **argv);
Weather *getWeather(void);

int main(int argc, char **argv)
{

	Weather *weather;

	weather = getWeather();

	WMInitializeApplication("wmforecast", &argc, argv);

	display = XOpenDisplay("");
	screen = WMCreateScreen(display, DefaultScreen(display));
	dockapp = WMCreateDockapp(screen, "", argc, argv);


	frame = WMCreateFrame(dockapp);
	WMSetFrameRelief(frame,WRSunken);
	WMResizeWidget(frame,56,56);
	WMSetWidgetBackgroundColor(frame,color("black"));
	WMRealizeWidget(frame);

	temp = WMCreateLabel(frame);
	WMSetLabelText(temp,concat(weather->temp,"°",NULL));
	WMSetWidgetBackgroundColor(temp,color("black"));
	WMSetLabelTextColor(temp,color("Light sea green"));
	WMSetLabelTextAlignment (temp, WACenter);
	WMResizeWidget(temp,52,14);
	WMMoveWidget(temp,2,40);
	WMRealizeWidget(temp);



	RContext *context;
	RImage *image;

	context = WMScreenRContext(screen);
	image = RLoadImage(context,"weather-few-clouds.png",0);
//	image = RSmoothScaleImage(image,32,32);

	iconPixmap = WMCreatePixmapFromRImage(screen,image,0);

	icon = WMCreateLabel(frame);
	WMSetWidgetBackgroundColor(icon,color("black"));
	WMRealizeWidget(icon);
	


	WMSetLabelImage(icon,iconPixmap);
	WMSetLabelImagePosition(icon,WIPImageOnly);
	WMResizeWidget(icon,32,32);
	WMMoveWidget(icon,12,5);

	WMSetBalloonTextForView("foo",WMWidgetView(icon)); 


	
	WMMapWidget(dockapp);
	WMMapWidget(frame);
	WMMapSubwidgets(frame);



	WMScreenMainLoop(screen);


}

WMWindow *WMCreateDockapp(WMScreen *screen, const char *name, int argc, char **argv)
{
	WMWindow *dockapp;
	XWMHints *hints;
	Display *display;
	Window window;
	
	display = WMScreenDisplay(screen);
	dockapp = WMCreateWindow(screen,name);
	WMRealizeWidget(dockapp);
	
	window = WMWidgetXID(dockapp);

	hints = XGetWMHints(display, window);
	hints->flags |= WindowGroupHint|IconWindowHint|StateHint;
	hints->window_group = window;
    	hints->icon_window = window;
    	hints->initial_state = WithdrawnState;

	XSetWMHints(display, window, hints);
	XFree(hints);

	XSetCommand(display, window, argv, argc);

	WMResizeWidget(dockapp,56,56);
	return dockapp;
}


/**************************************************
from http://curl.haxx.se/libcurl/c/getinmemory.html
***************************************************/
struct MemoryStruct {
	char *memory;
	size_t size;
};
 
 
static size_t
WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
 
	mem->memory = realloc(mem->memory, mem->size + realsize + 1);
	if(mem->memory == NULL) {
		/* out of memory! */ 
		printf("not enough memory (realloc returned NULL)\n");
		return 0;
	}
 
	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;
 
	return realsize;
}

struct MemoryStruct downloadWeather()
{
	CURL *curl_handle;
  	CURLcode res;
 	struct MemoryStruct chunk;
	chunk.memory = malloc(1);
	chunk.size = 0;
 
	curl_global_init(CURL_GLOBAL_ALL);
	curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_URL, "http://weather.yahooapis.com/forecastrss?p=61462");
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");
	res = curl_easy_perform(curl_handle);
	if(res != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
	}
	curl_easy_cleanup(curl_handle);
	curl_global_cleanup();
	return chunk;
}

Weather *getWeather(void)
{
	struct MemoryStruct chunk = downloadWeather();

	Weather *weather;

	weather = newWeather();

	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseMemory(chunk.memory, chunk.size);
	if (doc == NULL ) {
		fprintf(stderr,"Document not parsed successfully. \n");
		return;
	}
	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		fprintf(stderr,"empty document\n");
		xmlFreeDoc(doc);
		return;
	}
	
	if (xmlStrcmp(cur->name, (const xmlChar *) "rss")) {
		fprintf(stderr,"document of the wrong type, root node != rss");
		xmlFreeDoc(doc);
		return;
	}
		
	cur = cur->children;
	cur = cur->next;
	cur = cur->children;

	while (cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"item"))) {
			cur = cur->children;
			while (cur != NULL) {
				if ((!xmlStrcmp(cur->name, (const xmlChar *)"condition"))) {
					xmlChar *temp = xmlGetProp(cur,"temp");
					setTemp(weather,temp);
				}
				cur = cur->next;
			}
		}
		else cur = cur->next;
	}


	xmlFreeDoc(doc);
// finishing parsing xml

	if(chunk.memory)
		free(chunk.memory);

	return weather;
}


