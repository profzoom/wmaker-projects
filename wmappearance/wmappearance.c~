#include <WINGs/WINGs.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <glob.h>
#include <libgen.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>

#define MARGIN  14
#define BACKGROUNDBUTTON 128
#define SCROLLER_WIDTH 20
#define WINWIDTH 3*BACKGROUNDBUTTON+8*MARGIN+SCROLLER_WIDTH
#define WINHEIGHT 492
#define BUTTONWIDTH 60
#define BUTTONHEIGHT 24
#define BACKGROUNDBUTTON 128
#define TABOFFSET 31


Display *display;
WMScreen *screen;
WMWindow *win;



/***** Backgrounds tab variables ***********/
WMFrame *backgrounds;
WMFrame *backgroundsButtonFrame;
WMFrame *backgroundsImagesFrame;
WMButton *close;
WMButton *add;
WMButton *removeButton;
WMPopUpButton *backgroundStyleMenu;
WMPopUpButton *backgroundColorsMenu;
WMLabel *backgroundStyle;
WMLabel *backgroundColors;
WMScrollView *backgroundScroll;



WMTabView *tabview;
WMTabViewItem *tab;
WMLabel *themes;
WMLabel *styles;
WMLabel *icons;
WMPixmap *icon;

typedef struct
{
	char *filename;
	char *format;
	int width;
	int height;
	WMButton *button;
	int buttonX;
	int buttonY;
	char *balloontext;
	int index;
}
BackgroundOption;

BackgroundOption backgroundList[13];
BackgroundOption currentlySelectedBackground;


/* public domain string concatenation function by Solar Designer from popa3d */
char *concat(const char *s1, ...)
{
	va_list args;
	const char *s;
	char *p, *result;
	unsigned long l, m, n;
 
	m = n = strlen(s1);
	va_start(args, s1);
	while ((s = va_arg(args, char *))) {
		l = strlen(s);
		if ((m += l) < l) break;
	}
	va_end(args);
	if (s || m >= INT_MAX) return NULL;
 
	result = (char *)malloc(m + 1);
	if (!result) return NULL;
 
	memcpy(p = result, s1, n);
	p += n;
	va_start(args, s1);
	while ((s = va_arg(args, char *))) {
		l = strlen(s);
		if ((n += l) < l || n > m) break;
		memcpy(p, s, l);
		p += l;
	}
	va_end(args);
	if (s || m != n || p != result + n) {
		free(result);
		return NULL;
	}
 
	*p = 0;
	return result;
}

char *intToChar(int n)
{
	char *c = malloc((sizeof(int)*CHAR_BIT-1)/3 + 3);
	sprintf(c, "%d", n);
	return c;
}

void closeAll(WMWidget *self,void *data)
{
	WMDestroyWidget(self);
	exit(0);
}





/**********************************************************************
create a pixmap from file, scaled to fit inside button
*********************************************************************/
WMPixmap *WMCreatePixmapFromFileScaledForButton(WMScreen *screen,char *file)
{
	RContext *context;
	RImage *image;
	WMPixmap *pixmap;
	int height;
	int width;
	double aspectRatio;
	
	context = RCreateContext(display,DefaultScreen(display),NULL);
	image = RLoadImage(context,file,0);
	aspectRatio = ((double)image->width)/image->height;
	if (aspectRatio > 1)
	{
		width = fmin(BACKGROUNDBUTTON-2*MARGIN,image->width);
		height = width/aspectRatio;
	}
	else
	{
		height = fmin(BACKGROUNDBUTTON-2*MARGIN,image->height);
		width = height*aspectRatio;
	}
	image = RSmoothScaleImage(image,width,height);
	pixmap = WMCreatePixmapFromRImage(screen,image,0);
	return pixmap;
}	

void drawWidget(WMWidget *widget, int x, int y, int width, int height)
{
	WMMoveWidget(widget,x,y);
	WMResizeWidget(widget,width,height);
	WMRealizeWidget(widget);
	WMMapWidget(widget);
}

void moveBackgroundIcons(int x)
{
	int index;
	int maxButtons = (x-5*MARGIN-SCROLLER_WIDTH)/(MARGIN+BACKGROUNDBUTTON);
	int i,j;

	for(index = 0; index < sizeof(backgroundList)/sizeof(BackgroundOption); index++)
	{
		i = index % maxButtons;
		j = index / maxButtons;
		WMMoveWidget(backgroundList[index].button,MARGIN+i*(MARGIN+BACKGROUNDBUTTON),MARGIN+j*(MARGIN+BACKGROUNDBUTTON));
	}
	WMResizeWidget(backgroundsImagesFrame,x-4*MARGIN-SCROLLER_WIDTH,MARGIN+(j+1)*(MARGIN+BACKGROUNDBUTTON));
}


static void selectBackground(WMWidget *self,void *data)
{
	BackgroundOption *b = backgroundList;
	int i = 0;
	char *foo;
	for(;b->button != self;b++);
	WMSetButtonSelected(currentlySelectedBackground.button,0);
	system(concat("wmsetbg ",b->filename,NULL));
	currentlySelectedBackground = *b;	
}

BackgroundOption CreateBackgroundOptionFromFile(char *file,int index)
{
	BackgroundOption b;
	RContext *context;
	RImage *image;
	WMPixmap *pixmap;
	char *fileCopy;
	int maxButtons;
	int i;
	int j;
	
	b.filename = file;
	b.format = RGetImageFileFormat(file);
	context = RCreateContext(display,DefaultScreen(display),NULL);
	image = RLoadImage(context,file,0);
	b.width = image->width;
	b.height = image->height;
	b.index = index;


	b.button = WMCreateButton(backgroundsImagesFrame,WBTPushOnPushOff);
	pixmap = WMCreatePixmapFromFileScaledForButton(screen,file);
	WMSetButtonImagePosition(b.button,WIPImageOnly);
	WMSetButtonImage(b.button,pixmap);
	WMSetButtonAction(b.button, selectBackground, NULL);

	fileCopy = strdup(file); // avoids segfault with dirname
	b.balloontext = concat(basename(file),"\n",
			       b.format," image, ",
			       intToChar(b.width)," by ",intToChar(b.height)," pixels\n",
			       "Folder: ",dirname(fileCopy),NULL);
	WMSetBalloonTextForView(b.balloontext,WMWidgetView(b.button));

	maxButtons = (WINWIDTH-5*MARGIN)/(MARGIN+BACKGROUNDBUTTON);
	i = index % maxButtons;
	j = index / maxButtons;
	drawWidget(b.button,MARGIN+i*(MARGIN+BACKGROUNDBUTTON),MARGIN+j*(MARGIN+BACKGROUNDBUTTON),BACKGROUNDBUTTON,BACKGROUNDBUTTON);
	return b;
}


	

void getBackgroundIcons()
{
	glob_t backgroundFiles;
	size_t numFiles;
	int i;
	int k = 0;
	glob("/usr/share/WindowMaker/Backgrounds/*",0,NULL,&backgroundFiles);
	numFiles = backgroundFiles.gl_pathc;
	for (i = 0; i < numFiles; i++)
	{
		backgroundList[k] = CreateBackgroundOptionFromFile(backgroundFiles.gl_pathv[i],k);
		k++;
	}
	glob("/home/profzoom/GNUstep/Library/WindowMaker/Backgrounds/*",0,NULL,&backgroundFiles);
	numFiles = backgroundFiles.gl_pathc;
	for (i = 0; i < numFiles; i++)
	{
		backgroundList[k] = CreateBackgroundOptionFromFile(backgroundFiles.gl_pathv[i],k);
		k++;
	}
	glob("/usr/share/backgrounds/*",0,NULL,&backgroundFiles);
	numFiles = backgroundFiles.gl_pathc;
	for (i = 0; i < 7; i++)
	{
		backgroundList[k] = CreateBackgroundOptionFromFile(backgroundFiles.gl_pathv[i],k);
		k++;
	}
	currentlySelectedBackground = backgroundList[0];
	WMResizeWidget(backgroundsImagesFrame,WINWIDTH-4*MARGIN,MARGIN+k/3*(MARGIN+BACKGROUNDBUTTON));
		
}

/***********************************************************************
place all widgets in terms of current window size (for ease of resizing)
************************************************************************/
void placeWidgets(int x,int y)
{
	WMMoveWidget(close, x-BUTTONWIDTH-MARGIN,y-BUTTONHEIGHT-MARGIN);
	WMResizeWidget(tabview,x-2*MARGIN,y-3*MARGIN-BUTTONHEIGHT);
	WMMoveWidget(backgroundsButtonFrame,0,y-TABOFFSET-5*MARGIN-3*BUTTONHEIGHT);
	WMMoveWidget(add,x-3*MARGIN-BUTTONWIDTH,2*MARGIN+BUTTONHEIGHT);
	WMResizeWidget(backgroundsButtonFrame,x-2*MARGIN,100);
	WMMoveWidget(removeButton,x-4*MARGIN-2*BUTTONWIDTH,2*MARGIN+BUTTONHEIGHT);
	WMResizeWidget(backgroundScroll,x-4*MARGIN,y-6*MARGIN-3*BUTTONHEIGHT-TABOFFSET);
}

static void resizeHandler(void *self, WMNotification *notif)
{
	WMSize size = WMGetViewSize(WMWidgetView(win));
	placeWidgets(size.width,size.height);
	moveBackgroundIcons(size.width);
	
}


int main (int argc, char **argv)
{
  
	WMInitializeApplication("wmappearance", &argc, argv);

	display = XOpenDisplay("");
	screen = WMCreateScreen(display, DefaultScreen(display));
	icon = WMCreatePixmapFromFile(screen, "wmappearance.png");
	WMSetApplicationIconPixmap(screen, icon);

	win = WMCreateWindow(screen, "");
	WMSetWindowCloseAction(win, closeAll, NULL);
	WMSetWindowTitle(win, "Appearance Preferences");
	WMResizeWidget(win,WINWIDTH,WINHEIGHT);
	WMSetWindowMinSize(win,WINWIDTH,WINHEIGHT);
	WMSetViewNotifySizeChanges(WMWidgetView(win), True);
	WMAddNotificationObserver(resizeHandler, NULL, WMViewSizeDidChangeNotification, WMWidgetView(win));
	WMRealizeWidget(win);
	WMMapWidget(win);
   

	close = WMCreateButton(win, WBTMomentaryPush);
	WMSetButtonText(close,"Close");
	WMSetButtonAction(close, closeAll, NULL);
	WMMoveWidget(close,WINWIDTH-BUTTONWIDTH-MARGIN,WINHEIGHT-BUTTONHEIGHT-MARGIN);
	WMRealizeWidget(close);

	tabview = WMCreateTabView(win);
	WMMoveWidget(tabview,MARGIN,MARGIN);
	WMRealizeWidget(tabview);
 
	tab = WMCreateTabViewItemWithIdentifier(0);
	themes = WMCreateLabel(win);
	WMSetLabelText(themes,"Coming soon");
	WMRealizeWidget(themes);
	WMSetTabViewItemView(tab, WMWidgetView(themes));
	WMAddItemInTabView(tabview, tab);
	WMSetTabViewItemLabel(tab, "Themes");

	tab = WMCreateTabViewItemWithIdentifier(0);
	styles = WMCreateLabel(win);
	WMSetLabelText(styles,"Coming soon");
	WMRealizeWidget(styles);
	WMSetTabViewItemView(tab, WMWidgetView(styles));
	WMAddItemInTabView(tabview, tab);
	WMSetTabViewItemLabel(tab, "Styles");
 
	tab = WMCreateTabViewItemWithIdentifier(0);
	icons = WMCreateLabel(win);
	WMSetLabelText(icons,"Coming soon");
	WMRealizeWidget(icons);
	WMSetTabViewItemView(tab, WMWidgetView(icons));
	WMAddItemInTabView(tabview, tab);
	WMSetTabViewItemLabel(tab, "Icon Sets");

/********************************
Backgrounds Tab
********************************/
//main frame
	tab = WMCreateTabViewItemWithIdentifier(0);
	backgrounds = WMCreateFrame(win);
	WMSetFrameRelief(backgrounds, WRFlat);
	WMRealizeWidget(backgrounds);

//background images
	backgroundsImagesFrame = WMCreateFrame(backgrounds);
	WMSetFrameRelief(backgroundsImagesFrame, WRFlat);
	WMMoveWidget(backgroundsImagesFrame,MARGIN,MARGIN);

	backgroundScroll = WMCreateScrollView(backgrounds);
	WMSetScrollViewRelief(backgroundScroll,WRSimple);
	WMSetScrollViewHasHorizontalScroller(backgroundScroll,False);
	WMSetScrollViewHasVerticalScroller(backgroundScroll,True);
	WMSetScrollViewContentView(backgroundScroll,WMWidgetView(backgroundsImagesFrame));
	WMMoveWidget(backgroundScroll,MARGIN,MARGIN);
	WMResizeWidget(backgroundScroll,WINWIDTH-4*MARGIN,WINHEIGHT-6*MARGIN-TABOFFSET-3*BUTTONHEIGHT);
	WMRealizeWidget(backgroundScroll);
	WMMapWidget(backgroundScroll);
				   
	
	WMRealizeWidget(backgroundsImagesFrame);
	WMMapWidget(backgroundsImagesFrame);
	



	


	
	

//frame containing buttons on the bottom
	backgroundsButtonFrame = WMCreateFrame(backgrounds);
	WMSetFrameRelief(backgroundsButtonFrame,WRFlat);
	WMRealizeWidget(backgroundsButtonFrame);
	WMMapWidget(backgroundsButtonFrame);

// style picker
	backgroundStyle = WMCreateLabel(backgroundsButtonFrame);
	WMSetLabelText(backgroundStyle,"Style:");
	WMMoveWidget(backgroundStyle,MARGIN,MARGIN+4);
	WMRealizeWidget(backgroundStyle);

	backgroundStyleMenu = WMCreatePopUpButton(backgroundsButtonFrame);
	WMAddPopUpButtonItem(backgroundStyleMenu, "Tile");
	WMAddPopUpButtonItem(backgroundStyleMenu, "Scale");
	WMAddPopUpButtonItem(backgroundStyleMenu, "Center");
	WMAddPopUpButtonItem(backgroundStyleMenu, "Maximize");
	WMSetPopUpButtonSelectedItem(backgroundStyleMenu,0);
	WMMoveWidget(backgroundStyleMenu,MARGIN+40,MARGIN);
	WMResizeWidget(backgroundStyleMenu,72,BUTTONHEIGHT);
	WMRealizeWidget(backgroundStyleMenu);

// color picker
	backgroundColors = WMCreateLabel(backgroundsButtonFrame);
	WMSetLabelText(backgroundColors,"Colors:");
	WMMoveWidget(backgroundColors,2*MARGIN+112,MARGIN+4);
	WMRealizeWidget(backgroundColors);
	backgroundColorsMenu = WMCreatePopUpButton(backgroundsButtonFrame);
	WMAddPopUpButtonItem(backgroundColorsMenu, "None");
	WMAddPopUpButtonItem(backgroundColorsMenu, "Solid color");
	WMAddPopUpButtonItem(backgroundColorsMenu, "Diagonal gradient");
	WMAddPopUpButtonItem(backgroundColorsMenu, "Horizontal gradient");
	WMAddPopUpButtonItem(backgroundColorsMenu, "Vertical gradient");
	WMAddPopUpButtonItem(backgroundColorsMenu, "Interwoven gradient");
	WMSetPopUpButtonSelectedItem(backgroundColorsMenu,0);
	WMMoveWidget(backgroundColorsMenu,2*MARGIN+158,MARGIN);
	WMResizeWidget(backgroundColorsMenu,140,BUTTONHEIGHT);
	WMRealizeWidget(backgroundColorsMenu);

  
  

// add button
	add = WMCreateButton(backgroundsButtonFrame,WBTMomentaryPush);
	WMSetButtonText(add,"Add");
	WMRealizeWidget(add);


// remove button
	removeButton = WMCreateButton(backgroundsButtonFrame,WBTMomentaryPush);
	WMSetButtonText(removeButton,"Remove");
	WMSetButtonEnabled(removeButton,False); 
	WMRealizeWidget(removeButton);


	WMMapSubwidgets(backgroundsButtonFrame);

 

  
  

	WMSetTabViewItemView(tab, WMWidgetView(backgrounds));
	WMAddItemInTabView(tabview, tab);
	WMSetTabViewItemLabel(tab, "Backgrounds");
	WMSelectLastTabViewItem(tabview);

	placeWidgets(WINWIDTH,WINHEIGHT);
	getBackgroundIcons();

	WMMapSubwidgets(win);

	WMScreenMainLoop(screen);
}
